// HW17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>             
#include <ctime>  

using namespace std;

int main()

{
    setlocale(LC_ALL, "rus");
    auto now = time(nullptr);
    const auto ltm = localtime(&now);
    const int N = 8;
    int array[N][N];

    for (auto i = 0; i < N; i++)
    {
        for (auto j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    //������ ��� �������
    cout << "array: " << N << "x" << N << endl;
    for (auto i = 0; i < N; i++)
    {
        for (auto j = 0; j < N; j++)
        {
            if (i + j < 10)
                cout << " ";
            cout << array[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;

    //��������� ����� ����� �� ���������� ������� �� ������ ������� �� ������� ������� ���� � N
    static int number;
    for (auto i = 0; i < N; i++)
    {
        if (i == ltm->tm_mday % N)
            for (auto j = 0; j < N; j++)
            {
                number += array[i][j];
            }

        if (ltm->tm_mday % N == i)
            cout << "string: " << i << " number: " << number << " ";
    }
}

